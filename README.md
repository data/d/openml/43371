# OpenML dataset: New-Cases-of-COVID-19-In-World-Countries

https://www.openml.org/d/43371

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Has the curve flattened?
Countries around the world are working to flatten the curve of the coronavirus pandemic. Flattening the curve involves reducing the number of new COVID-19 cases from one day to the next. This helps prevent healthcare systems from becoming overwhelmed. When a country has fewer new COVID-19 cases emerging today than it did on a previous day, thats a sign that the country is flattening the curve.
On a trend line of total cases, a flattened curve looks how it sounds: flat. On the charts on this page, which show new cases per day, a flattened curve will show a downward trend in the number of daily new cases.
This analysis uses a 5-day moving average to visualize the number of new COVID-19 cases and calculate the rate of change. This is calculated for each day by averaging the values of that day, the two days before, and the two next days. This approach helps prevent major events (such as a change in reporting methods) from skewing the data. The interactive charts below show the daily number of new cases for the 10 most affected countries, based on the reported number of deaths by COVID-19.
This datas were last updated on Saturday, April 25, 2020 at 11:51 PM EDT.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43371) of an [OpenML dataset](https://www.openml.org/d/43371). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43371/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43371/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43371/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

